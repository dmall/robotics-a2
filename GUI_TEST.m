function varargout = GUI_TEST(varargin)
% GUI_TEST MATLAB code for GUI_TEST.fig
%      GUI_TEST, by itself, creates a new GUI_TEST or raises the existing
%      singleton*.
%
%      H = GUI_TEST returns the handle to a new GUI_TEST or the handle to
%      the existing singleton*.
%
%      GUI_TEST('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_TEST.M with the given input arguments.
%
%      GUI_TEST('Property','Value',...) creates a new GUI_TEST or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_TEST_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_TEST_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI_TEST

% Last Modified by GUIDE v2.5 06-Oct-2018 19:39:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_TEST_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_TEST_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI_TEST is made visible.
function GUI_TEST_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI_TEST (see VARARGIN)

% Choose default command line output for GUI_TEST
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI_TEST wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI_TEST_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function input1_Callback(hObject, eventdata, handles)
% hObject    handle to input1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of input1 as text
%        str2double(get(hObject,'String')) returns contents of input1 as a double


% --- Executes during object creation, after setting all properties.
function input1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to input1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function input2_Callback(hObject, eventdata, handles)
% hObject    handle to input2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of input2 as text
%        str2double(get(hObject,'String')) returns contents of input2 as a double


% --- Executes during object creation, after setting all properties.
function input2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to input2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in button.
function button_Callback(hObject, eventdata, handles)
% hObject    handle to button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)





function displayQ1_Callback(hObject, eventdata, handles)
% hObject    handle to displayQ1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of displayQ1 as text
%        str2double(get(hObject,'String')) returns contents of displayQ1 as a double

inputQ1 = str2num(get(handles.displayQ1,'String'))
assignin('base','userInput',inputQ1)
qMatrix = evalin('base','qMatrix');
cyton = evalin('base','cyton');
q1 = inputQ1; 
assignin('base','q1',q1);
qMatrix(1) = q1*0.0174533;
assignin('base','qMatrix',qMatrix);
cyton.model.plot(qMatrix,'trail','r-')
endEffectorCoord = cyton.model.fkine(qMatrix)
assignin('base','endEffectorCoord',endEffectorCoord)
set(handles.displayX,'String',num2str(endEffectorCoord(1,4)));
set(handles.displayY,'String',num2str(endEffectorCoord(2,4)));
set(handles.displayZ,'String',num2str(endEffectorCoord(3,4)));
rotMatrix = endEffectorCoord(1:3,1:3);
rpyMatrix = tr2rpy(rotMatrix); 
set(handles.displayR,'string',num2str(rpyMatrix(1)));
set(handles.displayPitch,'string',num2str(rpyMatrix(2)));
set(handles.displayYaw,'string',num2str(rpyMatrix(3)));

% --- Executes during object creation, after setting all properties.
function displayQ1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to displayQ1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function displayQ2_Callback(hObject, eventdata, handles)
% hObject    handle to displayQ2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of displayQ2 as text
%        str2double(get(hObject,'String')) returns contents of displayQ2 as a double

inputQ2 = str2num(get(handles.displayQ1,'String'))
assignin('base','userInput',inputQ2)
qMatrix = evalin('base','qMatrix');
cyton = evalin('base','cyton');
q2 = inputQ2; 
assignin('base','q2',q2);
qMatrix(2) = q2*0.0174533;
assignin('base','qMatrix',qMatrix);
cyton.model.plot(qMatrix,'trail','r-')
endEffectorCoord = cyton.model.fkine(qMatrix)
assignin('base','endEffectorCoord',endEffectorCoord)
set(handles.displayX,'String',num2str(endEffectorCoord(1,4)));
set(handles.displayY,'String',num2str(endEffectorCoord(2,4)));
set(handles.displayZ,'String',num2str(endEffectorCoord(3,4)));
rotMatrix = endEffectorCoord(1:3,1:3);
rpyMatrix = tr2rpy(rotMatrix); 
set(handles.displayR,'string',num2str(rpyMatrix(1)));
set(handles.displayPitch,'string',num2str(rpyMatrix(2)));
set(handles.displayYaw,'string',num2str(rpyMatrix(3)));



% --- Executes during object creation, after setting all properties.
function displayQ2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to displayQ2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function displayQ3_Callback(hObject, eventdata, handles)
% hObject    handle to displayQ3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of displayQ3 as text
%        str2double(get(hObject,'String')) returns contents of displayQ3 as a double

inputQ3 = str2num(get(handles.displayQ3,'String'))
assignin('base','userInput',inputQ3)
qMatrix = evalin('base','qMatrix');
cyton = evalin('base','cyton');
q3 = inputQ3; 
assignin('base','q3',q3);
qMatrix(3) = q3*0.0174533;
assignin('base','qMatrix',qMatrix);
cyton.model.plot(qMatrix,'trail','r-')
endEffectorCoord = cyton.model.fkine(qMatrix)
assignin('base','endEffectorCoord',endEffectorCoord)
set(handles.displayX,'String',num2str(endEffectorCoord(1,4)));
set(handles.displayY,'String',num2str(endEffectorCoord(2,4)));
set(handles.displayZ,'String',num2str(endEffectorCoord(3,4)));
rotMatrix = endEffectorCoord(1:3,1:3);
rpyMatrix = tr2rpy(rotMatrix); 
set(handles.displayR,'string',num2str(rpyMatrix(1)));
set(handles.displayPitch,'string',num2str(rpyMatrix(2)));
set(handles.displayYaw,'string',num2str(rpyMatrix(3)));



% --- Executes during object creation, after setting all properties.
function displayQ3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to displayQ3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function displayQ4_Callback(hObject, eventdata, handles)
% hObject    handle to displayQ4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of displayQ4 as text
%        str2double(get(hObject,'String')) returns contents of displayQ4 as a double

inputQ4 = str2num(get(handles.displayQ4,'String'))
assignin('base','userInput',inputQ4)
qMatrix = evalin('base','qMatrix');
cyton = evalin('base','cyton');
q4 = inputQ4; 
assignin('base','q4',q4);
qMatrix(4) = q4*0.0174533;
assignin('base','qMatrix',qMatrix);
cyton.model.plot(qMatrix,'trail','r-')
endEffectorCoord = cyton.model.fkine(qMatrix)
assignin('base','endEffectorCoord',endEffectorCoord)
set(handles.displayX,'String',num2str(endEffectorCoord(1,4)));
set(handles.displayY,'String',num2str(endEffectorCoord(2,4)));
set(handles.displayZ,'String',num2str(endEffectorCoord(3,4)));
rotMatrix = endEffectorCoord(1:3,1:3);
rpyMatrix = tr2rpy(rotMatrix); 
set(handles.displayR,'string',num2str(rpyMatrix(1)));
set(handles.displayPitch,'string',num2str(rpyMatrix(2)));
set(handles.displayYaw,'string',num2str(rpyMatrix(3)));


% --- Executes during object creation, after setting all properties.
function displayQ4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to displayQ4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function displayQ5_Callback(hObject, eventdata, handles)
% hObject    handle to displayQ5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of displayQ5 as text
%        str2double(get(hObject,'String')) returns contents of displayQ5 as a double

inputQ5 = str2num(get(handles.displayQ5,'String'))
assignin('base','userInput',inputQ5)
qMatrix = evalin('base','qMatrix');
cyton = evalin('base','cyton');
q5 = inputQ5; 
assignin('base','q5',q5);
qMatrix(5) = q5*0.0174533;
assignin('base','qMatrix',qMatrix);
cyton.model.plot(qMatrix,'trail','r-')
endEffectorCoord = cyton.model.fkine(qMatrix)
assignin('base','endEffectorCoord',endEffectorCoord)
set(handles.displayX,'String',num2str(endEffectorCoord(1,4)));
set(handles.displayY,'String',num2str(endEffectorCoord(2,4)));
set(handles.displayZ,'String',num2str(endEffectorCoord(3,4)));
rotMatrix = endEffectorCoord(1:3,1:3);
rpyMatrix = tr2rpy(rotMatrix); 
set(handles.displayR,'string',num2str(rpyMatrix(1)));
set(handles.displayPitch,'string',num2str(rpyMatrix(2)));
set(handles.displayYaw,'string',num2str(rpyMatrix(3)));


% --- Executes during object creation, after setting all properties.
function displayQ5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to displayQ5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function displayQ7_Callback(hObject, eventdata, handles)
% hObject    handle to displayQ7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of displayQ7 as text
%        str2double(get(hObject,'String')) returns contents of displayQ7 as a double

inputQ1 = str2num(get(handles.displayQ7,'String'))
assignin('base','userInput',inputQ7)
qMatrix = evalin('base','qMatrix');
cyton = evalin('base','cyton');
q7 = inputQ7; 
assignin('base','q7',q7);
qMatrix(7) = q7*0.0174533;
assignin('base','qMatrix',qMatrix);
cyton.model.plot(qMatrix,'trail','r-')
endEffectorCoord = cyton.model.fkine(qMatrix)
assignin('base','endEffectorCoord',endEffectorCoord)
set(handles.displayX,'String',num2str(endEffectorCoord(1,4)));
set(handles.displayY,'String',num2str(endEffectorCoord(2,4)));
set(handles.displayZ,'String',num2str(endEffectorCoord(3,4)));
rotMatrix = endEffectorCoord(1:3,1:3);
rpyMatrix = tr2rpy(rotMatrix); 
set(handles.displayR,'string',num2str(rpyMatrix(1)));
set(handles.displayPitch,'string',num2str(rpyMatrix(2)));
set(handles.displayYaw,'string',num2str(rpyMatrix(3)));


% --- Executes during object creation, after setting all properties.
function displayQ7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to displayQ7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function displayQ6_Callback(hObject, eventdata, handles)
% hObject    handle to displayQ6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of displayQ6 as text
%        str2double(get(hObject,'String')) returns contents of displayQ6 as a double

inputQ6 = str2num(get(handles.displayQ6,'String'))
assignin('base','userInput',inputQ6)
qMatrix = evalin('base','qMatrix');
cyton = evalin('base','cyton');
q6 = inputQ6; 
assignin('base','q6',q6);
qMatrix(1) = q6*0.0174533;
assignin('base','qMatrix',qMatrix);
cyton.model.plot(qMatrix,'trail','r-')
endEffectorCoord = cyton.model.fkine(qMatrix)
assignin('base','endEffectorCoord',endEffectorCoord)
set(handles.displayX,'String',num2str(endEffectorCoord(1,4)));
set(handles.displayY,'String',num2str(endEffectorCoord(2,4)));
set(handles.displayZ,'String',num2str(endEffectorCoord(3,4)));
rotMatrix = endEffectorCoord(1:3,1:3);
rpyMatrix = tr2rpy(rotMatrix); 
set(handles.displayR,'string',num2str(rpyMatrix(1)));
set(handles.displayPitch,'string',num2str(rpyMatrix(2)));
set(handles.displayYaw,'string',num2str(rpyMatrix(3)));



% --- Executes during object creation, after setting all properties.
function displayQ6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to displayQ6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function sliderQ1_Callback(hObject, eventdata, handles)
% hObject    handle to sliderQ1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

sliderVal1 = get(hObject,'Value');
assignin('base','sliderVal1',sliderVal1);
set(handles.displayQ1,'String',num2str(sliderVal1)); 
qMatrix = evalin('base','qMatrix');
cyton = evalin('base','cyton');
q1 = sliderVal1; 
assignin('base','q1',q1);
qMatrix(1) = q1*0.0174533;
assignin('base','qMatrix',qMatrix);
cyton.model.plot(qMatrix,'trail','r-')
endEffectorCoord = cyton.model.fkine(qMatrix)
assignin('base','endEffectorCoord',endEffectorCoord)
set(handles.displayX,'String',num2str(endEffectorCoord(1,4)));
set(handles.displayY,'String',num2str(endEffectorCoord(2,4)));
set(handles.displayZ,'String',num2str(endEffectorCoord(3,4)));
rotMatrix = endEffectorCoord(1:3,1:3);
rpyMatrix = tr2rpy(rotMatrix); 
set(handles.displayR,'string',num2str(rpyMatrix(1)));
set(handles.displayPitch,'string',num2str(rpyMatrix(2)));
set(handles.displayYaw,'string',num2str(rpyMatrix(3)));


% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderQ1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderQ1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sliderQ2_Callback(hObject, eventdata, handles)
% hObject    handle to sliderQ2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


sliderVal2 = get(hObject,'Value');
assignin('base','sliderVal1',sliderVal2);
set(handles.displayQ2,'String',num2str(sliderVal2)); 
qMatrix = evalin('base','qMatrix');
cyton = evalin('base','cyton');
q2 = sliderVal2; 
assignin('base','q2',q2);
qMatrix(2) = q2*0.0174533;
assignin('base','qMatrix',qMatrix);
cyton.model.plot(qMatrix,'trail','r-');
endEffectorCoord = cyton.model.fkine(qMatrix);
assignin('base','endEffectorCoord',endEffectorCoord);
set(handles.displayX,'String',num2str(endEffectorCoord(1,4)));
set(handles.displayY,'String',num2str(endEffectorCoord(2,4)));
set(handles.displayZ,'String',num2str(endEffectorCoord(3,4)));
rotMatrix = endEffectorCoord(1:3,1:3);
rpyMatrix = tr2rpy(rotMatrix); 
set(handles.displayR,'string',num2str(rpyMatrix(1)));
set(handles.displayPitch,'string',num2str(rpyMatrix(2)));
set(handles.displayYaw,'string',num2str(rpyMatrix(3)));



% --- Executes during object creation, after setting all properties.
function sliderQ2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderQ2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sliderQ3_Callback(hObject, eventdata, handles)
% hObject    handle to sliderQ3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

sliderVal3 = get(hObject,'Value');
assignin('base','sliderVal1',sliderVal3);
set(handles.displayQ3,'String',num2str(sliderVal3)); 
qMatrix = evalin('base','qMatrix');
cyton = evalin('base','cyton');
q3 = sliderVal3; 
assignin('base','q2',q3);
qMatrix(3) = q3*0.0174533;
assignin('base','qMatrix',qMatrix);
cyton.model.plot(qMatrix,'trail','r-')
endEffectorCoord = cyton.model.fkine(qMatrix)
assignin('base','endEffectorCoord',endEffectorCoord)
set(handles.displayX,'String',num2str(endEffectorCoord(1,4)));
set(handles.displayY,'String',num2str(endEffectorCoord(2,4)));
set(handles.displayZ,'String',num2str(endEffectorCoord(3,4)));
rotMatrix = endEffectorCoord(1:3,1:3);
rpyMatrix = tr2rpy(rotMatrix); 
set(handles.displayR,'string',num2str(rpyMatrix(1)));
set(handles.displayPitch,'string',num2str(rpyMatrix(2)));
set(handles.displayYaw,'string',num2str(rpyMatrix(3)));


% --- Executes during object creation, after setting all properties.
function sliderQ3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderQ3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sliderQ4_Callback(hObject, eventdata, handles)
% hObject    handle to sliderQ4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


sliderVal4 = get(hObject,'Value');
assignin('base','sliderVal1',sliderVal4);
set(handles.displayQ4,'String',num2str(sliderVal4)); 
qMatrix = evalin('base','qMatrix');
cyton = evalin('base','cyton');
q4 = sliderVal4; 
assignin('base','q2',q4);
qMatrix(4) = q4*0.0174533;
assignin('base','qMatrix',qMatrix);
cyton.model.plot(qMatrix,'trail','r-')
endEffectorCoord = cyton.model.fkine(qMatrix)
assignin('base','endEffectorCoord',endEffectorCoord)
set(handles.displayX,'String',num2str(endEffectorCoord(1,4)));
set(handles.displayY,'String',num2str(endEffectorCoord(2,4)));
set(handles.displayZ,'String',num2str(endEffectorCoord(3,4)));
rotMatrix = endEffectorCoord(1:3,1:3);
rpyMatrix = tr2rpy(rotMatrix); 
set(handles.displayR,'string',num2str(rpyMatrix(1)));
set(handles.displayPitch,'string',num2str(rpyMatrix(2)));
set(handles.displayYaw,'string',num2str(rpyMatrix(3)));



% --- Executes during object creation, after setting all properties.
function sliderQ4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderQ4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sliderQ5_Callback(hObject, eventdata, handles)
% hObject    handle to sliderQ5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


sliderVal5 = get(hObject,'Value');
assignin('base','sliderVal1',sliderVal5);
set(handles.displayQ5,'String',num2str(sliderVal5)); 
qMatrix = evalin('base','qMatrix');
cyton = evalin('base','cyton');
q5 = sliderVal5; 
assignin('base','q2',q5);
qMatrix(5) = q5*0.0174533;
assignin('base','qMatrix',qMatrix);
cyton.model.plot(qMatrix,'trail','r-')
endEffectorCoord = cyton.model.fkine(qMatrix)
assignin('base','endEffectorCoord',endEffectorCoord)
set(handles.displayX,'String',num2str(endEffectorCoord(1,4)));
set(handles.displayY,'String',num2str(endEffectorCoord(2,4)));
set(handles.displayZ,'String',num2str(endEffectorCoord(3,4)));
rotMatrix = endEffectorCoord(1:3,1:3);
rpyMatrix = tr2rpy(rotMatrix); 
set(handles.displayR,'string',num2str(rpyMatrix(1)));
set(handles.displayPitch,'string',num2str(rpyMatrix(2)));
set(handles.displayYaw,'string',num2str(rpyMatrix(3)));


% --- Executes during object creation, after setting all properties.
function sliderQ5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderQ5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sliderQ6_Callback(hObject, eventdata, handles)
% hObject    handle to sliderQ6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

sliderVal6 = get(hObject,'Value');
assignin('base','sliderVal1',sliderVal6);
set(handles.displayQ6,'String',num2str(sliderVal6)); 
qMatrix = evalin('base','qMatrix');
cyton = evalin('base','cyton');
q6 = sliderVal6; 
assignin('base','q2',q6);
qMatrix(6) = q6*0.0174533;
assignin('base','qMatrix',qMatrix);
cyton.model.plot(qMatrix,'trail','r-')
endEffectorCoord = cyton.model.fkine(qMatrix)
assignin('base','endEffectorCoord',endEffectorCoord)
set(handles.displayX,'String',num2str(endEffectorCoord(1,4)));
set(handles.displayY,'String',num2str(endEffectorCoord(2,4)));
set(handles.displayZ,'String',num2str(endEffectorCoord(3,4)));
rotMatrix = endEffectorCoord(1:3,1:3);
rpyMatrix = tr2rpy(rotMatrix); 
set(handles.displayR,'string',num2str(rpyMatrix(1)));
set(handles.displayPitch,'string',num2str(rpyMatrix(2)));
set(handles.displayYaw,'string',num2str(rpyMatrix(3)));



% --- Executes during object creation, after setting all properties.
function sliderQ6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderQ6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sliderQ7_Callback(hObject, eventdata, handles)
% hObject    handle to sliderQ7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

sliderVal7 = get(hObject,'Value');
assignin('base','sliderVal1',sliderVal7);
set(handles.displayQ7,'String',num2str(sliderVal7)); 
qMatrix = evalin('base','qMatrix');
cyton = evalin('base','cyton');
q7 = sliderVal7*0.0174533; 
assignin('base','q2',q7);
qMatrix(7) = q7;
assignin('base','qMatrix',qMatrix);
cyton.model.plot(qMatrix,'trail','r-')
endEffectorCoord = cyton.model.fkine(qMatrix)
assignin('base','endEffectorCoord',endEffectorCoord)
set(handles.displayX,'String',num2str(endEffectorCoord(1,4)));
set(handles.displayY,'String',num2str(endEffectorCoord(2,4)));
set(handles.displayZ,'String',num2str(endEffectorCoord(3,4)));
rotMatrix = endEffectorCoord(1:3,1:3);
rpyMatrix = tr2rpy(rotMatrix); 
set(handles.displayR,'string',num2str(rpyMatrix(1)));
set(handles.displayPitch,'string',num2str(rpyMatrix(2)));
set(handles.displayYaw,'string',num2str(rpyMatrix(3)));



% --- Executes during object creation, after setting all properties.
function sliderQ7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderQ7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end





% --- Executes on button press in pushButtonStart.
function pushButtonStart_Callback(hObject, eventdata, handles)

startGUI(handles);

function startGUI(handles)
cyton = Cyton();
cyton.PlotAndColourRobot();
assignin('base','cyton',cyton);
% 
% global q1;
% global q2;
% global q3;
% global q4;
% global q5;
% global q6;
% global q7;

q1 = 10;
q2 = 20;
q3 = 30;
q4 = 40; 
q5 = 50; 
q6 = 60;
q7 = 100;

assignin('base','q1',q1);
assignin('base','q2',q2);
assignin('base','q3',q3); 
assignin('base','q4',q4);
assignin('base','q5',q5);
assignin('base','q6',q6);
assignin('base','q7',q7);

% global qMatrix

qMatrix = deg2rad([q1 q2 q3 q4 q5 q6 q7]);
assignin('base','qMatrix',qMatrix);

axes(handles.axes2);
cyton.model.plot(qMatrix,'trail','r-')



function displayX_Callback(hObject, eventdata, handles)
% hObject    handle to displayX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of displayX as text
%        str2double(get(hObject,'String')) returns contents of displayX as a double

% set(handles.displayX,'String',num2str(sliderVal7)); 
qMatrix = evalin('base','qMatrix');
cyton = evalin('base','cyton');
assignin('base','qMatrix',qMatrix);
endEffectorCoord = cyton.model.fkine(qMatrix)
assignin('base','endEffectorCoord',endEffectorCoord)
set(handles.displayX,'String',num2str(endEffector(1,4)));


% --- Executes during object creation, after setting all properties.
function displayX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to displayX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function displayY_Callback(hObject, eventdata, handles)
% hObject    handle to displayY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of displayY as text
%        str2double(get(hObject,'String')) returns contents of displayY as a double


% --- Executes during object creation, after setting all properties.
function displayY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to displayY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function displayZ_Callback(hObject, eventdata, handles)
% hObject    handle to displayZ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of displayZ as text
%        str2double(get(hObject,'String')) returns contents of displayZ as a double


% --- Executes during object creation, after setting all properties.
function displayZ_CreateFcn(hObject, eventdata, handles)
% hObject    handle to displayZ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function displayR_Callback(hObject, eventdata, handles)
% hObject    handle to displayR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of displayR as text
%        str2double(get(hObject,'String')) returns contents of displayR as a double


% --- Executes during object creation, after setting all properties.
function displayR_CreateFcn(hObject, eventdata, handles)
% hObject    handle to displayR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function displayPitch_Callback(hObject, eventdata, handles)
% hObject    handle to displayPitch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of displayPitch as text
%        str2double(get(hObject,'String')) returns contents of displayPitch as a double


% --- Executes during object creation, after setting all properties.
function displayPitch_CreateFcn(hObject, eventdata, handles)
% hObject    handle to displayPitch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function displayYaw_Callback(hObject, eventdata, handles)
% hObject    handle to displayYaw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of displayYaw as text
%        str2double(get(hObject,'String')) returns contents of displayYaw as a double


% --- Executes during object creation, after setting all properties.
function displayYaw_CreateFcn(hObject, eventdata, handles)
% hObject    handle to displayYaw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushButtonDemo.
function pushButtonDemo_Callback(hObject, eventdata, handles)
% hObject    handle to pushButtonDemo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
hold on
cyton = Cyton();
axes(handles.axes2);
cyton.model.base = cyton.model.base * trotz(pi);
cyton.PlotAndColourRobot();
blockWidth = 0.025;
blockLength = 0.075;
blockThick = 0.015;
towerRef = [-0.13,0,0.2]; %Bottom right corner of tower 
% block reference point is at the centre of the block in all planes

% Display environment
table = Part(1,-0.3,-0.15,.01);
emergStop = Part(2,0.6,-0.1,0);
fireEx = Part(3,1,0.6,-0.4);
barrier1 = Part(4,0,0.5,-0.4);
barrier2 = Part(5,0,-0.4,-0.4);
table.model.plot(0);
emergStop.model.plot(0);
fireEx.model.plot(0);
barrier1.model.plot(0);
barrier2.model.plot(0);

%build each layer of tower
jenga11 = Part(11,towerRef(1,1)-0.5*blockWidth,towerRef(1,2)+0.5*blockLength,towerRef(1,3));
jenga12 = Part(12,towerRef(1,1)-1.5*blockWidth,towerRef(1,2)+0.5*blockLength,towerRef(1,3));
jenga13 = Part(13,towerRef(1,1)-2.5*blockWidth,towerRef(1,2)+0.5*blockLength,towerRef(1,3));

jenga21 = Part(21,towerRef(1,1)-1.5*blockWidth,towerRef(1,2)+0.5*blockWidth,towerRef(1,3)+blockThick);
jenga21.model.base = jenga21.model.base*trotz(-pi/2);
jenga22 = Part(22,towerRef(1,1)-1.5*blockWidth,towerRef(1,2)+1.5*blockWidth,towerRef(1,3)+blockThick);
jenga22.model.base = jenga22.model.base*trotz(-pi/2);
jenga23 = Part(23,towerRef(1,1)-1.5*blockWidth,towerRef(1,2)+2.5*blockWidth,towerRef(1,3)+blockThick);
jenga23.model.base = jenga23.model.base*trotz(-pi/2);

jenga31 = Part(31,towerRef(1,1)-0.5*blockWidth,towerRef(1,2)+0.5*blockLength,towerRef(1,3)+2*blockThick);
jenga32 = Part(32,towerRef(1,1)-1.5*blockWidth,towerRef(1,2)+0.5*blockLength,towerRef(1,3)+2*blockThick);
jenga33 = Part(33,towerRef(1,1)-2.5*blockWidth,towerRef(1,2)+0.5*blockLength,towerRef(1,3)+2*blockThick);

jenga41 = Part(41,towerRef(1,1)-1.5*blockWidth,towerRef(1,2)+0.5*blockWidth,towerRef(1,3)+3*blockThick);
jenga41.model.base = jenga41.model.base*trotz(-pi/2);
jenga42 = Part(42,towerRef(1,1)-1.5*blockWidth,towerRef(1,2)+1.5*blockWidth,towerRef(1,3)+3*blockThick);
jenga42.model.base = jenga42.model.base*trotz(-pi/2);
jenga43 = Part(43,towerRef(1,1)-1.5*blockWidth,towerRef(1,2)+2.5*blockWidth,towerRef(1,3)+3*blockThick);
jenga43.model.base = jenga43.model.base*trotz(-pi/2);

jenga11.model.plot(0);
jenga12.model.plot(0);
jenga13.model.plot(0);
jenga21.model.plot(0);
jenga22.model.plot(0);
jenga23.model.plot(0);
jenga31.model.plot(0);
jenga32.model.plot(0);
jenga33.model.plot(0);
jenga41.model.plot(0);
jenga42.model.plot(0);
jenga43.model.plot(0);

%move block on 3rd row and 3rd along out in order to grab it
jenga33.model.base = jenga33.model.base * transl(0,-0.3*blockLength,0);
jenga33.model.animate(0);

% Jenga peice pulled out
%poseCurrent = cyton.model.fkine(cyton.model.getpos()); %get current translation matrix of end effector
%put robot end effector at jenga piece 33

cyton.model.animate([-0.4381,1.1576, 1.1401,   -0.8910,    0.9550,   -1.6373,    1.9469]);
% Jenga peice pulled out
%put robot end effector at jenga piece 33
cyton.RMRC2(jenga33.model.base*transl(0,0.13,0),jenga33.model.base,[-pi/2,0,0],20);
poseCurrent = cyton.model.fkine(cyton.model.getpos());
%pull out jenga piece from tower
cyton.createLightCurtain();
cyton.RMRC2(poseCurrent,jenga33.model.base*transl(0,-0.09,0),[pi/2,0,0],20,jenga33,flyingObject);
poseCurrent = cyton.model.fkine(cyton.model.getpos());
%move jenga piece up vertically
cyton.RMRC2(poseCurrent,jenga33.model.base*transl(0,0,6*blockThick),[-pi/2,0,0],20,jenga33);
poseCurrent = cyton.model.fkine(cyton.model.getpos());
%movve jenga piece directly above tower pieces
cyton.RMRC2(poseCurrent,jenga13.model.base*transl(0,0,6*blockThick),[-pi/2,0,0],30,jenga33);
poseCurrent = cyton.model.fkine(cyton.model.getpos());
%move jenga piece down into position
cyton.RMRC2(poseCurrent,jenga13.model.base*transl(0.01,-0.008,3.9*blockThick),[-pi/2,-pi/25,0],30,jenga33);
poseCurrent = cyton.model.fkine(cyton.model.getpos());
%gripper off, move arm away
cyton.RMRC2(poseCurrent,jenga33.model.base*transl(0,-0.1,0),[-pi/2,-pi/10,0],20);
