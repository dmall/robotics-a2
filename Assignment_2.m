
% Assignment 2
rosinit('192.168.0.50');

%% jenga tower creation
close all
clear all
hold on

cyton = Cyton();
cyton.model.base = cyton.model.base * trotz(pi);
cyton.PlotAndColourRobot();

blockWidth = 0.025;
blockLength = 0.075;
blockThick = 0.015;
towerRef = [-0.13,0.08,0.2]; %Bottom right corner of tower

% block reference point is at the centre of the block in all planes

% Display environment
table = Part(1,-0.3,-0.15,.01);
emergStop = Part(2,0.6,-0.1,0);
fireEx = Part(3,1,0.6,-0.4);
barrier1 = Part(4,0,0.5,-0.4);
barrier2 = Part(5,0,-0.4,-0.4);
table.model.plot(0);
emergStop.model.plot(0);
fireEx.model.plot(0);
barrier1.model.plot(0);
barrier2.model.plot(0);

flyingObject = Part(8,-0.35,0.65,0.45);
flyingObject.model.base = flyingObject.model.base * trotz(pi/2);
flyingObject.model.plot(0);

%build each layer of tower
jenga11 = Part(11,towerRef(1,1)-0.5*blockWidth,towerRef(1,2)+0.5*blockLength,towerRef(1,3));
jenga12 = Part(12,towerRef(1,1)-1.5*blockWidth,towerRef(1,2)+0.5*blockLength,towerRef(1,3));
jenga13 = Part(13,towerRef(1,1)-2.5*blockWidth,towerRef(1,2)+0.5*blockLength,towerRef(1,3));

jenga21 = Part(21,towerRef(1,1)-1.5*blockWidth,towerRef(1,2)+0.5*blockWidth,towerRef(1,3)+blockThick);
jenga21.model.base = jenga21.model.base*trotz(-pi/2);
jenga22 = Part(22,towerRef(1,1)-1.5*blockWidth,towerRef(1,2)+1.5*blockWidth,towerRef(1,3)+blockThick);
jenga22.model.base = jenga22.model.base*trotz(-pi/2);
jenga23 = Part(23,towerRef(1,1)-1.5*blockWidth,towerRef(1,2)+2.5*blockWidth,towerRef(1,3)+blockThick);
jenga23.model.base = jenga23.model.base*trotz(-pi/2);

jenga31 = Part(31,towerRef(1,1)-0.5*blockWidth,towerRef(1,2)+0.5*blockLength,towerRef(1,3)+2*blockThick);
jenga32 = Part(32,towerRef(1,1)-1.5*blockWidth,towerRef(1,2)+0.5*blockLength,towerRef(1,3)+2*blockThick);
jenga33 = Part(33,towerRef(1,1)-2.5*blockWidth,towerRef(1,2)+0.5*blockLength,towerRef(1,3)+2*blockThick);

jenga41 = Part(41,towerRef(1,1)-1.5*blockWidth,towerRef(1,2)+0.5*blockWidth,towerRef(1,3)+3*blockThick);
jenga41.model.base = jenga41.model.base*trotz(-pi/2);
jenga42 = Part(42,towerRef(1,1)-1.5*blockWidth,towerRef(1,2)+1.5*blockWidth,towerRef(1,3)+3*blockThick);
jenga42.model.base = jenga42.model.base*trotz(-pi/2);
jenga43 = Part(43,towerRef(1,1)-1.5*blockWidth,towerRef(1,2)+2.5*blockWidth,towerRef(1,3)+3*blockThick);
jenga43.model.base = jenga43.model.base*trotz(-pi/2);

jenga11.model.plot(0);
jenga12.model.plot(0);
jenga13.model.plot(0);
jenga21.model.plot(0);
jenga22.model.plot(0);
jenga23.model.plot(0);
jenga31.model.plot(0);
jenga32.model.plot(0);
jenga33.model.plot(0);
jenga41.model.plot(0);
jenga42.model.plot(0);
jenga43.model.plot(0);

%move block on 3rd row and 3rd along out in order to grab it
jenga33.model.base = jenga33.model.base * transl(0,-0.3*blockLength,0);
jenga33.model.animate(0);
%% REAL ROBOT MOVEMENTS

cyton.initializeRobot();
cyton.enableGripper();
cyton.gripperPos(26); % 0 = closed, 26 = open max
cyton.model.animate([-0.4381,1.1576, 1.1401,   -0.8910,    0.9550,   -1.6373,    1.9469]);
% Jenga peice pulled out
%put robot end effector at jenga piece 33
cyton.RMRC(jenga33.model.base,jenga33.model.base*transl(0,0,-0.01),[-pi/2,0,-pi/16],8);
poseCurrent = cyton.model.fkine(cyton.model.getpos());
%pull out jenga piece from tower
cyton.gripperPos(12);
cyton.RMRC(poseCurrent,jenga33.model.base*transl(-0.01,-0.09,0),[-0.9*pi/2,0,-pi/16],5,jenga33);
poseCurrent = cyton.model.fkine(cyton.model.getpos());
%move jenga piece up vertically
cyton.RMRC(poseCurrent,jenga33.model.base*transl(0,0,6*blockThick),[-pi/2,0,0],5,jenga33);
poseCurrent = cyton.model.fkine(cyton.model.getpos());
%movve jenga piece directly above tower pieces
cyton.RMRC(poseCurrent,jenga13.model.base*transl(0,-.04,6*blockThick),[-pi/2,0,0],5,jenga33);
poseCurrent = cyton.model.fkine(cyton.model.getpos());
%move jenga piece down into position
cyton.RMRC(poseCurrent,jenga13.model.base*transl(0.01,-0.008,3.9*blockThick),[-pi/2,-pi/25,0],5,jenga33);
poseCurrent = cyton.model.fkine(cyton.model.getpos());
%gripper off, move arm away
cyton.gripperPos(26);
cyton.RMRC(poseCurrent,jenga33.model.base*transl(0,-0.1,0),[-pi/2,-pi/10,0],5);



%% SIMULATION MOVEMENTS

cyton.model.animate([-0.4381,1.1576, 1.1401,   -0.8910,    0.9550,   -1.6373,    1.9469]);
% Jenga peice pulled out
%put robot end effector at jenga piece 33
cyton.RMRC2(jenga33.model.base*transl(0,0.13,0),jenga33.model.base,[-pi/2,0,0],20);
poseCurrent = cyton.model.fkine(cyton.model.getpos());
%pull out jenga piece from tower
cyton.createLightCurtain();
cyton.RMRC2(poseCurrent,jenga33.model.base*transl(0,-0.09,0),[pi/2,0,0],20,jenga33,flyingObject);
poseCurrent = cyton.model.fkine(cyton.model.getpos());
%move jenga piece up vertically
cyton.RMRC2(poseCurrent,jenga33.model.base*transl(0,0,6*blockThick),[-pi/2,0,0],20,jenga33);
poseCurrent = cyton.model.fkine(cyton.model.getpos());
%movve jenga piece directly above tower pieces
cyton.RMRC2(poseCurrent,jenga13.model.base*transl(0,0,6*blockThick),[-pi/2,0,0],30,jenga33);
poseCurrent = cyton.model.fkine(cyton.model.getpos());
%move jenga piece down into position
cyton.RMRC2(poseCurrent,jenga13.model.base*transl(0.01,-0.008,3.9*blockThick),[-pi/2,-pi/25,0],30,jenga33);
poseCurrent = cyton.model.fkine(cyton.model.getpos());
%gripper off, move arm away
cyton.RMRC2(poseCurrent,jenga33.model.base*transl(0,-0.1,0),[-pi/2,-pi/10,0],20);

