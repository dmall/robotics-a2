%%

clear all 
clear 
clc 

%%

% cyton_sub = rossubscriber('/joint_states');
% pose_data = receive(cyton_sub,1)

%Enabling robot movement

%Setup Variables

cute_enable_robot_client = rossvcclient('enableCyton');
cute_enable_robot_msg = rosmessage(cute_enable_robot_client);

%To Enable/Disable the robot; Note: Hold onto the arm when disabling the robot.

cute_enable_robot_msg.TorqueEnable = true; %false
cute_enable_robot_client.call(cute_enable_robot_msg);

%%
%Enabling Gripper

%This is required for the real robot only and must be done before the gripper can be used.

%Setup Variables

cute_enable_gripper_client = rossvcclient('claw_controller/torque_enable')
cute_enable_gripper_msg = rosmessage(cute_enable_gripper_client);

%To Enable/Disable the gripper

cute_enable_gripper_msg.TorqueEnable = 1 %0
cute_enable_gripper_client.call(cute_enable_gripper_msg);

%%
%Create a subscriber to view the current state of each joint

%Setup Variables

stateSub = rossubscriber('/joint_states');
receive(stateSub,2)
msg = stateSub.LatestMessage;

%To show the joint angles and end effector position:

msg.JointAngles
msg.Pos

%%
%To move each joint to a specific angle

%Creating Variables

cute_move_client = rossvcclient('/GoToPosition');
cute_move_msg = rosmessage(cute_move_client);

%Creating the position message (Example 0 rad, 0 rad, 0 rad, 0 rad, 0 rad, 0 rad, 0 rad) (i.e. the home position) And command the robot to move to this location

cute_move_msg.JointStates = [0,0,0,0,0,0,0];
cute_move_client.call(cute_move_msg);
%%
%To move a single joint to specific angle

%Setup Variables

cute_single_joint_client = rossvcclient('/MoveSingleJoint');
cute_single_joint_msg = rosmessage(cute_single_joint_client);

%Setting values and sending to the robot

cute_single_joint_msg.JointNumber = 1;% Joints 0-6
cute_single_joint_msg.Position = 1.1;% (Rads)
cute_single_joint_client.call(cute_single_joint_msg);

%%
%To move robot home

%Setup Variables

cute_home_client = rossvcclient('/goHome');
cute_home_client_msg = rosmessage(cute_home_client);

%Setting values and sending to the robot

cute_home_client.call(cute_home_client_msg);

%Open and Close the claw

%Setup Variables

claw_client = rossvcclient('/ClawPosition');
claw_msg = rosmessage(claw_client);

%Publishing

claw_msg.Data = 0; % Values must be between 0 (closed) and 26.5 (open) ~ a millimeter value of the claw open amount.
claw_client.call(claw_msg);

%PUBLISHERS
%Velocity Control of Joint Angles

%NOTE: To send velocities to the cyton, the speed (in rad/s) must be sent to the controller at a rate higher than 10hz - Please contact Gavin if this is unachievable. Be aware of joint limits as the robot will need to be reset if it exceeds these limits

%Setup Publisher and Message

cute_velocity_publisher = rospublisher('/cyton_velocity_commands');
cute_velocity_msg = rosmessage(cute_velocity_publisher);

%Publishing Velocities; Example: moving joint 1 at 0.1 rad/s with all other joints at 0 rad/s

    cute_velocity_msg.Data = [0.1,0,0,0,0,0,0];
    cute_velocity_publisher.send(cute_velocity_msg);

%Position Control of Joint Angles

%Setup Publisher and Message

cute_position_publisher = rospublisher('/cyton_position_commands');
cute_position_msg = rosmessage(cute_position_publisher);

    cute_position_msg.Data = [0.5,0,0,0,0,0,0];
    cute_position_publisher.send(cute_position_msg);

