classdef Cyton < handle
    
    properties
        model;
        workspace = [-0.5 0.5 -0.5 0.5 -0.2 0.5];
        qNeutral = [0, 0, 0, 0, 0, 0, 0]; % pre-defined neutral psition
        lightCurtainPoints = [-0.3, -0.2, 0.53; -0.3,0.4,0.53]; %dictates the location of the top row of light curtain
        curtainPoints1; % curtainPoints dictate start and end of individual light rays in light curtain
        curtainPoints2;
        numLightBeams = 11;
        curtainLength = 0.5;    %legnth of light curtain in Z direction
        
    end
    
    
    
    methods
        function self = Cyton()
            self.GetCyton;
            self.PlotRobot;
        end
        
        function GetCyton(self)
            %creates a Cyton Robot model
            pause(0.001);
            
            % D-H parameters
            L1 = Link('d',0.157,'a',0,'alpha',pi/2,'offset',-pi/2,'qlim',[deg2rad(-145),deg2rad(145)]);
            L2 = Link('d',0,'a',0,'alpha',-pi/2,'offset',0,'qlim',[deg2rad(-100),deg2rad(100)]);
            L3 = Link('d',0.1408,'a',0,'alpha',pi/2,'offset',0,'qlim',[deg2rad(-145),deg2rad(145)]);
            L4 = Link('d',0,'a',0.0718,'alpha',-pi/2,'offset',pi/2,'qlim',[deg2rad(-100),deg2rad(100)]);
            L5 = Link('d',0,'a',0.0718,'alpha',pi/2,'offset',0,'qlim',[deg2rad(-100),deg2rad(100)]);
            L6 = Link('d',0,'a',0,'alpha',-pi/2,'offset',-pi/2,'qlim',[deg2rad(-100),deg2rad(100)]);
            L7 = Link('d',0.11,'a',0,'alpha',0,'offset',-pi/2,'qlim',[deg2rad(-145),deg2rad(145)]);
            
            self.model = SerialLink([L1 L2 L3 L4 L5 L6 L7],'name','coolrobot');   
            
        end
        
        
        function PlotRobot(self)
            self.model.plot(self.qNeutral,'workspace',self.workspace,'noarrow','scale',0.2);%,'trail','-r');
            
        end
        
        %% Movement of robot and parts
        
        function qMatrix = CalcTraj(self,q1,q2,steps)
            s = lspb(0,1,steps);           % First, create the scalar function
            qMatrix = nan(steps,7);        % Create memory allocation for variables
            for i = 1:steps
                qMatrix(i,:) = (1-s(i))*q1 + s(i)*q2; % Generate interpolated joint angles
            end
        end
        
        function MoveTo(self,trans,steps)   %trans is the 4x4 transformation matrix of final planned position
            robotQ = self.model.getpos();
            robotQNew = self.model.ikcon(trans);
            qMatrix = self.CalcTraj(robotQ,robotQNew,steps);
            %animate each set of joint angles generated from CalcTraj
            for i = 1:steps
                self.model.animate(qMatrix(i,:));
            end
        end
        
        function MoveWithPart(self,object,trans,steps)
            %trans is a 4x4 transformation matrix
            robotQ = self.model.getpos();
            robotQNew = self.model.ikcon(trans);
            qMatrix = self.CalcTraj(robotQ,robotQNew,steps);
            %log taken of transform between current joint angles and new joint
            %angles of robot
            for i = 1:steps
                coord = self.model.fkine(qMatrix(i,:));
                object.model.base = coord*trotx(pi);
                object.model.animate(0);
                self.model.animate(qMatrix(i,:));
                
            end
        end
        
        function RMRC(self,startTransl,endTransl,RPY,steps,varargin)
            startXYZ = startTransl(1:3,4)';
            endXYZ = endTransl(1:3,4)';
            t = 5;                              % Total time
            deltaT = t/steps;                   % Discrete time step
            delta = 2*pi/steps;                 % Small angle change
            epsilon = 0.05;                     % Threshold value for manipulability/Damped Least Squares
            W = diag([1 1 1 0.1 0.1 0.1]);      % Weighting matrix for the velocity vector
            
            % Allocate array data
            m = zeros(steps,1);                 % Array for Measure of Manipulability
            qMatrix = zeros(steps,7);           % Array for joint angles
            qdot = zeros(steps,7);              % Array for joint velocities
            theta = zeros(3,steps);             % Array for roll-pitch-yaw angles
            x = zeros(3,steps);                 % Array for x-y-z trajectory
            positionError = zeros(3,steps);     % For plotting trajectory error
            angleError = zeros(3,steps);        % For plotting trajectory error
            
            % Trajectory setup
            s = lspb(0,1,steps);                % Trapezoidal trajectory scalar
            for i=1:steps
                x(1,i) = (1-s(i))*startXYZ(1,1) + s(i)*endXYZ(1,1);     % X direction movement
                x(2,i) = (1-s(i))*startXYZ(1,2) + s(i)*endXYZ(1,2);     % Y direction movement
                x(3,i) = (1-s(i))*startXYZ(1,3) + s(i)*endXYZ(1,3);     % Z direction movement
                theta(1,i) = RPY(1,1);                                  % Roll angle
                theta(2,i) = RPY(1,2);                                  % Pitch angle
                theta(3,i) = RPY(1,3);                                  % Yaw angle
            end
            
            T = [rpy2r(theta(1,1),theta(2,1),theta(3,1)) x(:,1);zeros(1,3) 1];   % Transformation of first point and angle
            q0 = zeros(1,7);
            qMatrix(1,:) = self.model.getpos();% self.model.ikcon(T,q0);                               % First waypoint
            % Track the trajectory with RMRC
            for i = 1:steps-1
                T = self.model.fkine(qMatrix(i,:));                     % Get forward transformation at current joint state
                deltaX = x(:,i+1) - T(1:3,4);                           % Change to next position from current position
                Rd = rpy2r(theta(1,i+1),theta(2,i+1),theta(3,i+1));     % Get next RPY angles, convert to rotation matrix
                Ra = T(1:3,1:3);                                        % Current end-effector rotation matrix
                Rdot = (1/deltaT)*(Rd - Ra);                            % Calculate rotation matrix error
                S = Rdot*Ra';                                           % Skew symmetric equation rearranged
                linear_velocity = (1/deltaT)*deltaX;
                angular_velocity = [S(3,2);S(1,3);S(2,1)];              % Extract velocities from skew symmetric
                deltaTheta = tr2rpy(Rd*Ra');                            % Convert rotation matrix to RPY angles
                xdot = W*[linear_velocity;angular_velocity];            % Calculate end-effector velocity to reach next waypoint.
                J = self.model.jacob0(qMatrix(i,:));                    % Get Jacobian at current joint state
                m(i) = sqrt(det(J*J'));                                 % maniputability
                if m(i) < epsilon                                       % If manipulability is less than given threshold
                    lambda = (1 - m(i)/epsilon)*5E-3;                   % Damping coefficient for Damped Least Squares
                else
                    lambda = 0;                                         % dont use Damped Least Squares
                end
                invJ = inv(J'*J + lambda *eye(7))*J';                   % DLS Inverse
                qdot(i,:) = (invJ*xdot)';                               % Solve the RMRC equation
                for j = 1:7                                             % Loop through joints 1 to 7
                    if qMatrix(i,j) + deltaT*qdot(i,j) < self.model.qlim(j,1)       % If next joint angle is lower than joint limit...
                        qdot(i,j) = 0;                                              % Stop the motor
                    elseif qMatrix(i,j) + deltaT*qdot(i,j) > self.model.qlim(j,2)   % If next joint angle is greater than joint limit ...
                        qdot(i,j) = 0;                                              % Stop the motor
                    end
                end
                qMatrix(i+1,:) = qMatrix(i,:) + deltaT*qdot(i,:);       % Update next joint state based on joint velocities
                
                positionError(:,i) = deltaX;                            % For plotting
                angleError(:,i) = deltaTheta;                           % For plotting
            end
            
            % Plotting movement
            figure(1)
            cute_move_client = rossvcclient('/GoToPosition');
            cute_move_msg = rosmessage(cute_move_client);
            if nargin == 5                                  %if no part supplied, move robot only
                for i = 1:steps
                    self.model.animate(qMatrix(i,:));
                    cute_move_msg.JointStates = qMatrix(i,:);
                    cute_move_client.call(cute_move_msg);
                end
            end
            
            
            %controls both robot and part
            if nargin == 6
                for i = 1:steps
                    self.model.animate(qMatrix(i,:));
                    varargin{1}.model.base = self.model.fkine(qMatrix(i,:))*trotx(pi/2);
                    varargin{1}.model.animate(0);
                    cute_move_msg.JointStates = [qMatrix(i,:)];
                    cute_move_client.call(cute_move_msg);
                end
            end
        end
        
        function RMRC2(self,startTransl,endTransl,RPY,steps,varargin)
            %% resolved motion rate control - Simulation ONLY
            startXYZ = startTransl(1:3,4)';
            endXYZ = endTransl(1:3,4)';
            t = 5;                              % Total time
            deltaT = t/steps;                   % Discrete time step
            delta = 2*pi/steps;                 % Small angle change
            epsilon = 0.05;                     % Threshold value for manipulability/Damped Least Squares
            W = diag([1 1 1 0.1 0.1 0.1]);      % Weighting matrix for the velocity vector
            
            % Allocate array data
            m = zeros(steps,1);                 % Array for Measure of Manipulability
            qMatrix = zeros(steps,7);           % Array for joint angles
            qdot = zeros(steps,7);              % Array for joint velocities
            theta = zeros(3,steps);             % Array for roll-pitch-yaw angles
            x = zeros(3,steps);                 % Array for x-y-z trajectory
            positionError = zeros(3,steps);     % For plotting trajectory error
            angleError = zeros(3,steps);        % For plotting trajectory error
            
            % Trajectory setup
            s = lspb(0,1,steps);                % Trapezoidal trajectory scalar
            for i=1:steps
                x(1,i) = (1-s(i))*startXYZ(1,1) + s(i)*endXYZ(1,1);     % X direction movement
                x(2,i) = (1-s(i))*startXYZ(1,2) + s(i)*endXYZ(1,2);     % Y direction movement
                x(3,i) = (1-s(i))*startXYZ(1,3) + s(i)*endXYZ(1,3);     % Z direction movement
                theta(1,i) = RPY(1,1);                                  % Roll angle
                theta(2,i) = RPY(1,2);                                  % Pitch angle
                theta(3,i) = RPY(1,3);                                  % Yaw angle
            end
            
            T = [rpy2r(theta(1,1),theta(2,1),theta(3,1)) x(:,1);zeros(1,3) 1];   % Transformation of first point and angle
            q0 = zeros(1,7);
            qMatrix(1,:) = self.model.getpos();% self.model.ikcon(T,q0);                               % First waypoint
            % Track the trajectory with RMRC
            for i = 1:steps-1
                T = self.model.fkine(qMatrix(i,:));                     % Get forward transformation at current joint state
                deltaX = x(:,i+1) - T(1:3,4);                           % Change to next position from current position
                Rd = rpy2r(theta(1,i+1),theta(2,i+1),theta(3,i+1));     % Get next RPY angles, convert to rotation matrix
                Ra = T(1:3,1:3);                                        % Current end-effector rotation matrix
                Rdot = (1/deltaT)*(Rd - Ra);                            % Calculate rotation matrix error
                S = Rdot*Ra';                                           % Skew symmetric equation rearranged
                linear_velocity = (1/deltaT)*deltaX;
                angular_velocity = [S(3,2);S(1,3);S(2,1)];              % Extract velocities from skew symmetric
                deltaTheta = tr2rpy(Rd*Ra');                            % Convert rotation matrix to RPY angles
                xdot = W*[linear_velocity;angular_velocity];            % Calculate end-effector velocity to reach next waypoint.
                J = self.model.jacob0(qMatrix(i,:));                    % Get Jacobian at current joint state
                m(i) = sqrt(det(J*J'));                                 % maniputability
                if m(i) < epsilon                                       % If manipulability is less than given threshold
                    lambda = (1 - m(i)/epsilon)*5E-3;                   % Damping coefficient for Damped Least Squares
                else
                    lambda = 0;                                         % dont use Damped Least Squares
                end
                invJ = inv(J'*J + lambda *eye(7))*J';                   % DLS Inverse
                qdot(i,:) = (invJ*xdot)';                               % Solve the RMRC equation
                for j = 1:7                                             % Loop through joints 1 to 7
                    if qMatrix(i,j) + deltaT*qdot(i,j) < self.model.qlim(j,1)       % If next joint angle is lower than joint limit...
                        qdot(i,j) = 0;                                              % Stop the motor
                    elseif qMatrix(i,j) + deltaT*qdot(i,j) > self.model.qlim(j,2)   % If next joint angle is greater than joint limit ...
                        qdot(i,j) = 0;                                              % Stop the motor
                    end
                end
                qMatrix(i+1,:) = qMatrix(i,:) + deltaT*qdot(i,:);       % Update next joint state based on joint velocities
                
                positionError(:,i) = deltaX;                            % For plotting
                angleError(:,i) = deltaTheta;                           % For plotting
            end
            
            % Plotting movement
            figure(1)
            if nargin == 5                                  %if no part supplied, move robot only
                for i = 1:steps
                    self.model.animate(qMatrix(i,:));                    
                end
            end
            
            
            %controls both robot and part
            if nargin == 6
                for i = 1:steps
                    self.model.animate(qMatrix(i,:));
                    varargin{1}.model.base = self.model.fkine(qMatrix(i,:))*trotx(pi/2);
                    varargin{1}.model.animate(0);
                    
                end
            end
            
            if nargin == 7
                for i = 1:steps
                    self.model.animate(qMatrix(i,:));
                    varargin{1}.model.base = self.model.fkine(qMatrix(i,:))*trotx(pi/2);
                    varargin{1}.model.animate(0);
                
                    varargin{2}.model.base = varargin{2}.model.base*transl(-0.2/(0.5*steps),0,0);
                    varargin{2}.model.animate(0);
                    collision = self.checkCurtainCollision(varargin{2});
                    while collision == 1
                        varargin{2}.model.base = varargin{2}.model.base*transl(-0.4/(0.5*steps),0,0);
                        varargin{2}.model.animate(0);
                        collision = self.checkCurtainCollision(varargin{2});
                    end
                    
                end
            end
        end
        
        
        
        
        function InitialMove(self,startTransl,endTransl,RPY,steps)
            startXYZ = startTransl(1:3,4)';
            endXYZ = endTransl(1:3,4)';
            t = 5;                              % Total time
            deltaT = t/steps;                   % Discrete time step
            delta = 2*pi/steps;                 % Small angle change
            epsilon = 0.05;                     % Threshold value for manipulability/Damped Least Squares
            W = diag([1 1 1 0.1 0.1 0.1]);      % Weighting matrix for the velocity vector
            
            % Allocate array data
            m = zeros(steps,1);                 % Array for Measure of Manipulability
            qMatrix = zeros(steps,7);           % Array for joint angles
            qdot = zeros(steps,7);              % Array for joint velocities
            theta = zeros(3,steps);             % Array for roll-pitch-yaw angles
            x = zeros(3,steps);                 % Array for x-y-z trajectory
            positionError = zeros(3,steps);     % For plotting trajectory error
            angleError = zeros(3,steps);        % For plotting trajectory error
            
            % Trajectory setup
            s = lspb(0,1,steps);                % Trapezoidal trajectory scalar
            for i=1:steps
                x(1,i) = (1-s(i))*startXYZ(1,1) + s(i)*endXYZ(1,1);     % X direction movement
                x(2,i) = (1-s(i))*startXYZ(1,2) + s(i)*endXYZ(1,2);     % Y direction movement
                x(3,i) = (1-s(i))*startXYZ(1,3) + s(i)*endXYZ(1,3);     % Z direction movement
                theta(1,i) = RPY(1,1);                                  % Roll angle
                theta(2,i) = RPY(1,2);                                  % Pitch angle
                theta(3,i) = RPY(1,3);                                  % Yaw angle
            end
            
            T = [rpy2r(theta(1,1),theta(2,1),theta(3,1)) x(:,1);zeros(1,3) 1];   % Transformation of first point and angle
            q0 = zeros(1,7);
            qMatrix(1,:) = self.model.ikcon(T,q0);                               % First waypoint
            % Track the trajectory with RMRC
            for i = 1:steps-1
                T = self.model.fkine(qMatrix(i,:));                     % Get forward transformation at current joint state
                deltaX = x(:,i+1) - T(1:3,4);                           % Change to next position from current position
                Rd = rpy2r(theta(1,i+1),theta(2,i+1),theta(3,i+1));     % Get next RPY angles, convert to rotation matrix
                Ra = T(1:3,1:3);                                        % Current end-effector rotation matrix
                Rdot = (1/deltaT)*(Rd - Ra);                            % Calculate rotation matrix error
                S = Rdot*Ra';                                           % Skew symmetric equation rearranged
                linear_velocity = (1/deltaT)*deltaX;
                angular_velocity = [S(3,2);S(1,3);S(2,1)];              % Extract velocities from skew symmetric
                deltaTheta = tr2rpy(Rd*Ra');                            % Convert rotation matrix to RPY angles
                xdot = W*[linear_velocity;angular_velocity];            % Calculate end-effector velocity to reach next waypoint.
                J = self.model.jacob0(qMatrix(i,:));                    % Get Jacobian at current joint state
                m(i) = sqrt(det(J*J'));                                 % maniputability
                if m(i) < epsilon                                       % If manipulability is less than given threshold
                    lambda = (1 - m(i)/epsilon)*5E-3;                   % Damping coefficient for Damped Least Squares
                else
                    lambda = 0;                                         % dont use Damped Least Squares
                end
                invJ = inv(J'*J + lambda *eye(7))*J';                   % DLS Inverse
                qdot(i,:) = (invJ*xdot)';                               % Solve the RMRC equation
                for j = 1:7                                             % Loop through joints 1 to 7
                    if qMatrix(i,j) + deltaT*qdot(i,j) < self.model.qlim(j,1)       % If next joint angle is lower than joint limit...
                        qdot(i,j) = 0;                                              % Stop the motor
                    elseif qMatrix(i,j) + deltaT*qdot(i,j) > self.model.qlim(j,2)   % If next joint angle is greater than joint limit ...
                        qdot(i,j) = 0;                                              % Stop the motor
                    end
                end
                qMatrix(i+1,:) = qMatrix(i,:) + deltaT*qdot(i,:);       % Update next joint state based on joint velocities
                
                positionError(:,i) = deltaX;                            % For plotting
                angleError(:,i) = deltaTheta;                           % For plotting
            end
            
            % Plotting movement
            figure(1)
            for i = 1:steps
                self.model.animate(qMatrix(i,:));
                
            end
                    
        end
        %%
        function CalcVolume(self)
            
            radStep = deg2rad(45);
            qLimits = self.model.qlim;
            pointCloudSize = prod(floor((qLimits(1:6,2)-qLimits(1:6,1))/radStep + 1));
            pointCloud = zeros(pointCloudSize,3);
            %declaring array of the correct size - quicker than editing the
            %size of array each iteration of the below loops
            counter = 1;
            tic;
            for q1 = qLimits(1,1):radStep:qLimits(1,2)
                for q2 = qLimits(2,1):radStep:qLimits(2,2)
                    for q3 = qLimits(3,1):radStep:qLimits(3,2)
                        for q4 = qLimits(4,1):radStep:qLimits(4,2)
                            for q5 = qLimits(5,1):radStep:qLimits(5,2)
                                for q6 = qLimits(6,1):radStep:qLimits(6,2)
                                    % joint 7 affects end-effector yaw only,
                                    % therefore leave it as zero
                                    q7 = 0;
                                    
                                    q = [q1,q2,q3,q4,q5,q6,q7];
                                    tr = self.model.fkine(q);
                                    pointCloud(counter,:) = tr(1:3,4)';
                                    counter = counter + 1;
                                end
                            end
                        end
                    end
                end
            end
            plot3(pointCloud(:,1),pointCloud(:,2),pointCloud(:,3),'r.');
            %volume = (4/3)*pi*(radius^3)
            %display the total volume of arm positions
        end
        %%
        function PlotAndColourRobot(self)
            for linkIndex = 1:self.model.n
                [ faceData, vertexData, plyData{linkIndex + 1} ] = plyread(['Cyton',num2str(linkIndex),'.ply'],'tri'); %#ok<AGROW>
                self.model.faces{linkIndex + 1} = faceData;
                self.model.points{linkIndex + 1} = vertexData;
            end
            % Display robot
            self.model.plot3d(zeros(1,self.model.n),'noarrow','workspace',self.workspace);
            %             if isempty(findobj(get(gca,'Children'),'Type','Light'))
            camlight

            self.model.delay = 0;
            for linkIndex = 1:self.model.n
                handles = findobj('Tag', self.model.name);
                h = get(handles,'UserData');
                try
                    h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                        , plyData{linkIndex+1}.vertex.green ...
                        , plyData{linkIndex+1}.vertex.blue]/255;
                    h.link(linkIndex+1).Children.FaceColor = 'interp';
                catch ME_1
                    disp(ME_1);
                    continue;
                end
            end
        end
        
        
        function createLightCurtain(self)
            % interperet curtain as a number of light beams
            curtainPoint1 = zeros(self.numLightBeams,3);
            curtainPoint2 = zeros(self.numLightBeams,3);
            for i = 1:self.numLightBeams
                %formula to spread points out evenly over numLightBeams
                % x-(x-y)((i-1)(num-1))
                curtainPoint1(i,:) = self.lightCurtainPoints(1,:) - (self.lightCurtainPoints(1,:)-self.lightCurtainPoints(2,:))*((i-1)/(self.numLightBeams-1));
                curtainPoint2(i,:) = curtainPoint1(i,:)-[0,0,self.curtainLength];
            end
            % plot area of light curtain usin rectangular prism class
            plotOptions.plotFaces = true;
            RectangularPrism(curtainPoint1(1,:),curtainPoint2(self.numLightBeams,:),plotOptions);
            
            self.curtainPoints1 = curtainPoint1;
            self.curtainPoints2 = curtainPoint2;
            
        end
        
        function collision = checkCurtainCollision(self,object)
            %collision = 0; % set collision variable to no colllision
            % check if any beams intersect plane of object
            % object is simplified into circle on x-y plane
            for i = 1:self.numLightBeams
                collision = 0;
                [intersectionPt, check] = LinePlaneIntersection([0,0,1],object.model.base(1:3,4)',self.curtainPoints1(i,:),self.curtainPoints2(i,:));
                if check == 1 %intersection of line and plane of object
                    
                    % determine if line intersects in vacinity of object
                    % finding distance of object to line in XY plane
                    objectLineDistance = sqrt((self.curtainPoints1(i,1)-object.model.base(1,4))^2 + (self.curtainPoints1(i,2)-object.model.base(2,4))^2);
                    if objectLineDistance < 0.1 % assumed radius of object
                        collision = 1; % object collision with light curtain
                        return
                    end
                else
                    collision = 0;
                end
            end

            
            
        end
        
        function enableGripper(self)
            
            cute_enable_gripper_client = rossvcclient('/claw_controller/torque_enable');
            cute_enable_gripper_msg = rosmessage(cute_enable_gripper_client);
            
            %To Enable/Disable the gripper
            
            cute_enable_gripper_msg.TorqueEnable = 1; %0
            cute_enable_gripper_client.call(cute_enable_gripper_msg);
            
        end
        
        function gripperPos(self,dist)
            
            claw_client = rossvcclient('/ClawPosition');
            claw_msg = rosmessage(claw_client);
            
            %Publishing
            
            claw_msg.Data = dist; % Values must be between 0 (closed) and 26.5 (open) ~ a millimeter value of the claw open amount.
            claw_client.call(claw_msg);
            
        end
        
        function initializeRobot(self)
            
            cute_enable_robot_client = rossvcclient('enableCyton');
            cute_enable_robot_msg = rosmessage(cute_enable_robot_client);
            
            cute_enable_robot_msg.TorqueEnable = true; %false
            cute_enable_robot_client.call(cute_enable_robot_msg);
        end
    end
    
end