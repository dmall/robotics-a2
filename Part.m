classdef Part < handle
    
    properties      
        % A cell structure of models
        model;
              
        % Dimensions of the workspace
        workspaceDimensions;
        
        % Ply file data of each part
        faceData = [];  
        vertexData = [];
        plyData = [];   
    end
    
    methods
 
        function self = Part(partNum,x,y,z)
            %coordinates of object are set when constructing object

            self.model = self.GetModel(['part',num2str(partNum)],partNum);
            self.model.base = transl(x,y,z);
            % Plot 3D model
            %self.model.animate(0);            
            %axis equal
            %camlight;
        end     
        
        
        %% GetModel
        function model = GetModel(self,name,partNum)
            if isempty(self.faceData) || isempty(self.vertexData) || isempty(self.plyData)
                [self.faceData,self.vertexData,self.plyData] = plyread(['part',num2str(partNum),'.ply'],'tri');
                %ply file data of parts is stored for each part
            end
            L1 = Link('alpha',0,'a',0,'d',0.075,'offset',0);
            model = SerialLink(L1,'name',name);
            %1 link robot used to simulate parts for simplicity
            model.faces = {self.faceData,[]};
            model.points = {self.vertexData,[]};
                                   
            plot3d(model,0,'workspace',self.workspaceDimensions,'delay',0);

            
%                     handles = findobj('Tag', self.model.name);
%         h = get(handles,'UserData');
%         try 
%             h.link(1).Children.FaceVertexCData = [plyData.vertex.red ...
%                                                           , plyData.vertex.green ...
%                                                           , plyData.vertex.blue]/255;
%             h.link(1).Children.FaceColor = 'interp';
%         catch ME_1
%             disp(ME_1);
%             
%             
%             
%         end
        
        end
        
    end    
end

